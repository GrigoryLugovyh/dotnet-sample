﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace prefix_tree_search
{
    public class Program
    {
        public static void Main()
        {
            BenchmarkRunner.Run<BenchmarkAutoCompleter>();
        }

        [RankColumn]
        [MemoryDiagnoser]
        [Orderer(BenchmarkDotNet.Order.SummaryOrderPolicy.FastestToSlowest)]
        public class BenchmarkAutoCompleter
        {
            private const string Search = "кисе";
            
            private readonly AutoCompleterTree _completerTree;
            private readonly AutoCompleterContains _completerContains;
        
            public BenchmarkAutoCompleter()
            {
                var input = File.ReadAllLines("full_names.txt", Encoding.UTF8).ToArray();

                _completerTree = new AutoCompleterTree();
                _completerTree.AddToSearch(input);
                
                _completerContains = new AutoCompleterContains();
                _completerContains.AddToSearch(input);
            }
        
            [Benchmark]
            public void RunAutoCompleterTree()
            {
                var _ = _completerTree.Search(Search);
            }

            [Benchmark]
            public void RunAutoCompleterStartWith()
            {
                var _ = _completerContains.Search(Search);
            }
        }
    }

    public class AutoCompleterContains
    {
        private readonly List<string> _fullNameStorage = new();

        public void AddToSearch(string[] fullNames)
        {
            _fullNameStorage.AddRange(fullNames);
        }

        public List<string> Search(string prefix)
        {
            return _fullNameStorage.Where(name => name.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase))
                .ToList();
        }
    }

    public class AutoCompleterTree
    {
        private const string Separator = " ";
        
        private readonly PrefixTreeNode _prefixTree;
        private readonly Dictionary<int, string> _namesIndex;

        public AutoCompleterTree()
        {
            _prefixTree = new PrefixTreeNode();
            _namesIndex = new Dictionary<int, string>();
        }

        public void AddToSearch(string[] fullNames)
        {
            foreach (var fullName in fullNames)
            {
                var preparedName = FormatFullName(fullName);

                var nameIndex = preparedName.GetHashCode();
                var stackName = new Stack<char>(preparedName.ToLower()
                    .Replace(Separator, string.Empty)
                    .Reverse());

                _prefixTree.Add(stackName, nameIndex);
                
                if (!_namesIndex.ContainsKey(nameIndex))
                    _namesIndex.Add(nameIndex, preparedName);
            }
        }

        public List<string> Search(string prefix)
        {
            var result = _prefixTree.Search(new Stack<char>(prefix
                .Replace(Separator, string.Empty)
                .ToLowerInvariant()
                .Reverse()));

            if (result.Count == 0)
                return new List<string>();

            return result.Select(i => _namesIndex[i]).ToList();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string FormatFullName(string fullName)
        {
            return string.Join(Separator,
                fullName.ToLower()
                    .Split(Separator, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => char.ToUpper(x[0]) + x[1..]));
        }
    }

    public class PrefixTreeNode
    {
        private readonly Dictionary<char, PrefixTreeNode> _nodes = new();
        private readonly HashSet<int> _pointers = new();

        public void Add(Stack<char> name, int wordIndex)
        {
            _pointers.Add(wordIndex);

            if (name.Count == 0)
                return;

            var c = name.Pop();

            if (_nodes.ContainsKey(c))
            {
                _nodes[c].Add(name, wordIndex);
                return;
            }

            var node = new PrefixTreeNode();
            node.Add(name, wordIndex);
            _nodes.Add(c, node);
        }

        public HashSet<int> Search(Stack<char> search)
        {
            if (search.Count == 0)
                return _pointers;

            var c = search.Pop();

            if (_nodes.ContainsKey(c))
                return _nodes[c].Search(search);

            return new HashSet<int>();
        }
    }
}