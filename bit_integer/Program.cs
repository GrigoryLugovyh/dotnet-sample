﻿using System;
using System.Linq;

namespace bit_integer
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new BigInteger("1");
            var b = new BigInteger("2");

            var c = a + b;

            Console.WriteLine(c);
            Console.ReadLine();
        }
    }

    public class BigInteger
    {
        public int[] Digints { get; }

        public BigInteger(string input)
        {
            if (string.IsNullOrEmpty(input)) throw new ArgumentException();

            if (input.StartsWith('0') && input.Length > 1) throw new ArgumentException();

            Digints = input.Select(c => (int) char.GetNumericValue(c)).ToArray();
            Array.Reverse(Digints);
        }

        public static BigInteger operator +(BigInteger left, BigInteger right)
        {
            var r = string.Empty;
            var c = 0;

            for (var i = 0;
                i < (left.Digints.Length > right.Digints.Length ? left.Digints.Length : right.Digints.Length);
                i++)
            {
                var a = i < left.Digints.Length ? left.Digints[i] : 0;
                var b = i < right.Digints.Length ? right.Digints[i] : 0;

                var x = a + b + c;

                if (x >= 10)
                {
                    r = (x - 10) + r;
                    c = 1;
                    continue;
                }

                r = x + r;
                c = 0;
            }

            if (c > 0)
                r = c + r;

            return new BigInteger(r);
        }

        public override string ToString()
        {
            if (Digints == null || Digints.Length == 0)
                return string.Empty;

            var clone = (int[]) Digints.Clone();

            Array.Reverse(clone);

            return string.Join(string.Empty, clone);
        }
    }
}
