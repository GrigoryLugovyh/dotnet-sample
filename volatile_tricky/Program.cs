﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace volatile_tricky
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //new VolatileIsTrickyTest_Fail().Test();
            new VolatileIsTrickyTest_Success().Test();
        }
    }

    /// <summary>
    /// there are reordeing operations
    /// </summary>
    internal class VolatileIsTrickyTest_Fail
    {
        private volatile bool A;
        private volatile bool A_Won;
        
        private volatile bool B;
        private volatile bool B_Won;

        private void ThreadA()
        {
            A = true;
            if (!B)
                A_Won = true;
        }

        private void ThreadB()
        {
            B = true;
            if (!A)
                B_Won = true;
        }

        public void Test()
        {
            for (var i = 0; i < 10_000; i++)
            {
                A = B = A_Won = B_Won = false;

                var t1 = Task.Run(ThreadA);
                var t2 = Task.Run(ThreadB);
                Task.WaitAll(t1, t2);

                if (i % 10 == 0)
                    Console.WriteLine($"{i} iterations done");

                if (A_Won && B_Won)
                    throw new Exception("That's impossible!");
            }
        }
    }
    
    /// <summary>
    /// MemoryBarrier exclude reordering operation on cpu
    /// </summary>
    internal class VolatileIsTrickyTest_Success
        {
        private volatile bool A;
        private volatile bool A_Won;
        
        private volatile bool B;
        private volatile bool B_Won;

        private void ThreadA()
        {
            A = true;
            Thread.MemoryBarrier(); // <<<<<<<<<<<<<<<<<<<<<
            if (!B)
                A_Won = true;
        }

        private void ThreadB()
        {
            B = true;
            Thread.MemoryBarrier(); // <<<<<<<<<<<<<<<<<<<<<
            if (!A)
                B_Won = true;
        }

        public void Test()
        {
            for (var i = 0; i < 10_000; i++)
            {
                A = B = A_Won = B_Won = false;

                var t1 = Task.Run(ThreadA);
                var t2 = Task.Run(ThreadB);
                Task.WaitAll(t1, t2);

                if (i % 10 == 0)
                    Console.WriteLine($"{i} iterations done");

                if (A_Won && B_Won)
                    throw new Exception("That's impossible!");
            }
        }
    }
}