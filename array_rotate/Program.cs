﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace array_rotate
{
    class Program
    {
        static void Main(string[] args)
        {
            void WriteArray(string description, IEnumerable<int> a) =>
                Console.WriteLine($"{description}: {string.Join(", ", a)}");

            var array = Enumerable.Range(1, 10).Select(i => i).ToArray();
            WriteArray("before ronate:", array);

            Shift(array, -5);
            WriteArray("after rotate:", array);

            Console.ReadLine();
        }


        /// <summary>
        /// Элегантный сдвиг массива с линейной скоростью и константной памятью в три сторки
        /// </summary>
        /// <param name="input"></param>
        /// <param name="direction"></param>
        public static void Shift(int[] input, int direction)
        {
            input.AsSpan(0, direction).Reverse();
            input.AsSpan(direction).Reverse();
            input.AsSpan().Reverse();
        }
    }
}
