﻿using System.Reflection;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Order;
using BenchmarkDotNet.Running;

namespace reflection_speed_up;

public class Program
{
    public static void Main()
    {
        BenchmarkRunner.Run<Benchmark>();
    }

    [RankColumn]
    [MemoryDiagnoser]
    [Orderer(SummaryOrderPolicy.FastestToSlowest)]
    public class Benchmark
    {
        [Benchmark]
        public void InstanceCall()
        {
            var c = new ClassWithPublicProp();
            _ = c.Fuuu;
        }
        
        [Benchmark]
        public void ReflactionCall()
        {
            var c = new ClassWithPrivateProp();
            _ = c.GetType().GetProperty("Fuuu", BindingFlags.NonPublic | BindingFlags.Instance)!.GetValue(c);
        }

        private static readonly PropertyInfo FuuPropInfo =
            typeof(ClassWithPrivateProp).GetProperty("Fuuu", BindingFlags.NonPublic | BindingFlags.Instance)!;
        
        [Benchmark]
        public void ReflactionCallWithMethodCache()
        {
            var c = new ClassWithPrivateProp();
            _ = FuuPropInfo.GetValue(c);
        }

        private static readonly Func<ClassWithPrivateProp, string> FuuPropDelegate =
            (Func<ClassWithPrivateProp, string>) Delegate.CreateDelegate(
                typeof(Func<ClassWithPrivateProp, string>),
                FuuPropInfo.GetGetMethod(true)!
            );

        [Benchmark]
        public void ReflactionCallWithMethodDelegateCache()
        {
            var c = new ClassWithPrivateProp();
            _ = FuuPropDelegate(c);
        }
    }
}

public class ClassWithPublicProp
{
    public string Fuuu => "public_fuu";
}

public class ClassWithPrivateProp
{
    private string Fuuu => "private_fuu";
}