﻿using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace lock_free_stack
{
    internal class Program
    {
        private static void Main()
        {
            BenchmarkRunner.Run<StackComparer>();
        }
    }

    [MemoryDiagnoser]
    public class StackComparer
    {
        [Benchmark]
        public void LockFreeStack()
        {
            var stack = new LockFreeStack<int>();
            Parallel.ForEach(Enumerable.Range(1, 5000), new ParallelOptions { MaxDegreeOfParallelism = 8 }, i =>
            {
                if (i % 2 != 0)
                    stack.Push(i);
                else
                    stack.TryPop(out _);
            });
        }

        [Benchmark]
        public void ConcurrentStack()
        {
            var stack = new ConcurrentStack<int>();
            Parallel.ForEach(Enumerable.Range(1, 5000), new ParallelOptions {MaxDegreeOfParallelism = 8}, i =>
            {
                if (i % 2 != 0)
                    stack.Push(i);
                else
                    stack.TryPop(out _);
            });
        }
    }

    public class LockFreeStack<T>
    {
        private volatile Node _head;

        private class Node { public Node Next; public T Value; }

        public void Push(T item)
        {
            var spin = new SpinWait();
            var node = new Node {Value = item};

            while (true)
            {
                var head = _head;
                node.Next = head;
                if (Interlocked.CompareExchange(ref _head, node, head) == head) break;
                spin.SpinOnce();
            }
        }

        public bool TryPop(out T result)
        {
            result = default(T);
            var spin = new SpinWait();

            while (true)
            {
                var head = _head;
                if (head == null) return false;

                if (Interlocked.CompareExchange(ref _head, head.Next, head) == head)
                {
                    result = head.Value;
                    return true;
                }

                spin.SpinOnce();
            }
        }
    }
}
