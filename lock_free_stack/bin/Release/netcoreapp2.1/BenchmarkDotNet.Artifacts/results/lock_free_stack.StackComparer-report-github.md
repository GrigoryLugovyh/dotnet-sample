``` ini

BenchmarkDotNet=v0.11.1, OS=Windows 10.0.17134.345 (1803/April2018Update/Redstone4)
Intel Core i7-4700MQ CPU 2.40GHz (Haswell), 1 CPU, 8 logical and 4 physical cores
Frequency=2338346 Hz, Resolution=427.6527 ns, Timer=TSC
.NET Core SDK=2.1.403
  [Host]     : .NET Core 2.1.5 (CoreCLR 4.6.26919.02, CoreFX 4.6.26919.02), 64bit RyuJIT
  DefaultJob : .NET Core 2.1.5 (CoreCLR 4.6.26919.02, CoreFX 4.6.26919.02), 64bit RyuJIT


```
|          Method |      Mean |     Error |    StdDev | Rank |   Gen 0 | Allocated |
|---------------- |----------:|----------:|----------:|-----:|--------:|----------:|
|   LockFreeStack |  2.453 ms | 0.0485 ms | 0.1033 ms |    1 | 35.1563 |  30.34 KB |
| ConcurrentStack | 12.162 ms | 0.2546 ms | 0.4254 ms |    2 | 31.2500 |  32.79 KB |
