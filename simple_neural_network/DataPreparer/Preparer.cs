﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using simple_neural_network.Utils;

namespace simple_neural_network.DataPreparer;

[SuppressMessage("Interoperability", "CA1416:Проверка совместимости платформы")]
public static class Preparer
{
    public static void PrepareData()
    {
        var files = Directory.GetFiles("c:\\temp");

        foreach (var file in files)
            PrepareInternal(file);
    }

    public static IEnumerable<Bitmap> PrepareFile(string file)
    {
        using var originalImage = BitmapUtils.LoadBitmap(file);

        const int totalSlices = 4;
        var width = originalImage.Width / 6;

        for (var i = 0; i < totalSlices; i++)
        {
            var rect = new Rectangle(i * width, 0, width, originalImage.Height);
            var slice = originalImage.Clone(rect, originalImage.PixelFormat);
            yield return slice;
        }
    }

    public static void SaveDataToFile(string data, string filePath)
    {
        var bytes = Convert.FromBase64String(data);
        using var imageFile = new FileStream(filePath, FileMode.Create);
        imageFile.Write(bytes, 0, bytes.Length);
        imageFile.Flush();
    }

    private static void PrepareInternal(string file)
    {
        foreach (var bitmap in PrepareFile(file))
            bitmap.Save($"c:\\temp\\slices\\{StringUtils.GenerateShortString()}.jpg");
    }
}