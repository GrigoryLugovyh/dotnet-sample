﻿using System.Diagnostics;
using simple_neural_network.DataGetter;
using simple_neural_network.DataPreparer;
using simple_neural_network.NeuralNetwork;
using simple_neural_network.Utils;

namespace simple_neural_network.DataProcessor;

public static class Processor
{
    public static async Task Process(IEnumerable<string> inns)
    {
        foreach (var inn in inns)
        {
            await Process(inn);
        }
    }

    public static async Task Process(string inn)
    {
        var watcher = Stopwatch.StartNew();
        
        var attempts = 0;
        
        do
        {
            var captchaFilePath = $"c:\\temp\\process\\{StringUtils.GenerateShortString()}.jpg";

            Console.WriteLine("getting new captcha ...");

            var inputCaptcha = await Getter.GetCaptcha();
            Preparer.SaveDataToFile(inputCaptcha.Data, captchaFilePath);
            var captcha = NeuralNetworkPredictor.Predict(captchaFilePath);

            Console.WriteLine($"captcha resolved {captcha}");

            var checkResult = await Getter.CheckInn(new CheckInnRequest(inn, captcha));
        
            if (!checkResult.IsError)
            {
                Console.WriteLine($"{inn}: {checkResult.IsSuccess} for {watcher.ElapsedMilliseconds} ms\n");
                break;
            }

            attempts++;

            if (attempts > 3)
            {
                Console.WriteLine("ERROR: cant check inn");
                break;
            }
            
        } while (true);
        
        watcher.Stop();
    }
}