﻿namespace simple_neural_network.Utils;

public class StringUtils
{
    private static readonly DateTime StartDateTimeValue = new(2016, 1, 1);

    public static string GenerateShortString()
    {
        return (DateTime.Now.Ticks - StartDateTimeValue.Ticks).ToString("x").ToLower();
    }
}