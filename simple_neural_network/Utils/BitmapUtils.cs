﻿using System.Drawing;
using System.Drawing.Imaging;

namespace simple_neural_network.Utils;

public static class BitmapUtils
{
    public static Bitmap LoadBitmap(string file)
    {
        return new Bitmap(Image.FromFile(file));
    }

    private static Bitmap ToMonoChrome(Bitmap bitmap)
    {
        var bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly,
            PixelFormat.Format1bppIndexed);

        var newBitmap = new Bitmap(bitmap.Width, bitmap.Height, bmpData.Stride, PixelFormat.Format1bppIndexed,
            bmpData.Scan0);

        return newBitmap;
    }
}