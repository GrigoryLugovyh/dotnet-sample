﻿using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using simple_neural_network.DataPreparer;
using simple_neural_network.Utils;

namespace simple_neural_network.DataGetter;

public record CaptchaResult(bool IsError, string Data);

public record CheckInnRequest(string Inn, string Captcha);

public record CheckInnResult(bool IsError, bool IsSuccess);

public static class Getter
{
    private static readonly HttpClient client;

    static Getter()
    {
        client = new HttpClient();
        client.DefaultRequestHeaders.Add("Cookie",
            "_ym_d=1657958678; _ym_uid=1657958678294662320; ASP.NET_SessionId=oiropuk54r0pek4jggj2etop");
        client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0");
        client.DefaultRequestHeaders.Add("Accept", "application/json");
    }

    public static async Task<CaptchaResult> GetCaptcha()
    {
        var response = await client.PostAsync("https://portal.fedsfm.ru/Captcha/RefreshCaptcha", null);
        var captcha = await response.Content.ReadFromJsonAsync<CaptchaResult>();
        return captcha!;
    }

    public static async Task<CheckInnResult> CheckInn(CheckInnRequest request)
    {
        var response = await client.PostAsync("https://portal.fedsfm.ru/check-inn/check",
            new StringContent(JsonSerializer.Serialize(request), Encoding.UTF8, "application/json"));
        var result = await response.Content.ReadFromJsonAsync<CheckInnResult>();
        return result!;
    }

    public static async Task GetData(int count)
    {
        while (count > 0)
        {
            var captcha = await GetCaptcha();
            Preparer.SaveDataToFile(captcha!.Data, $"c:\\temp\\class_test\\{StringUtils.GenerateShortString()}.jpg");
            count--;
        }
    }
}