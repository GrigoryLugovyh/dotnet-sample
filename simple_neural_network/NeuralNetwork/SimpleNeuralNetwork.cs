﻿namespace simple_neural_network.NeuralNetwork;

public class SimpleNeuralNetwork
{
    private readonly Func<double, double> _activation;
    private readonly Func<double, double> _derivative;
    private readonly double _learningRate;

    public SimpleNeuralNetwork(double learningRate, Func<double, double> activation, Func<double, double> derivative,
        params int[] sizes)
    {
        _learningRate = learningRate;
        _activation = activation;
        _derivative = derivative;

        Layers = new SimpleLayer[sizes.Length];

        for (var i = 0; i < sizes.Length; i++)
        {
            var nextSize = 0;
            if (i < sizes.Length - 1)
                nextSize = sizes[i + 1];

            Layers[i] = new SimpleLayer(sizes[i], nextSize);

            for (var j = 0; j < sizes[i]; j++)
            {
                Layers[i].Biases[j] = Random.Shared.NextDouble() * 2.0 - 1.0;

                for (var k = 0; k < nextSize; k++)
                    Layers[i].Weights[j, k] = Random.Shared.NextDouble() * 2.0 - 1.0;
            }
        }
    }

    public SimpleLayer[] Layers { get; set; }

    public double[] Predict(double[] inputs)
    {
        Array.Copy(inputs, 0, Layers[0].Neurons, 0, inputs.Length);

        for (var i = 1; i < Layers.Length; i++)
        {
            var l = Layers[i - 1];
            var l1 = Layers[i];

            for (var j = 0; j < l1.Size; j++)
            {
                l1.Neurons[j] = 0;

                for (var k = 0; k < l.Size; k++)
                    l1.Neurons[j] += l.Neurons[k] * l.Weights[k, j];

                l1.Neurons[j] += l1.Biases[j];
                l1.Neurons[j] = _activation(l1.Neurons[j]);
            }
        }

        return Layers[^1].Neurons;
    }

    public void Correct(double[] targets)
    {
        var errors = new double[Layers[^1].Size];

        for (var i = 0; i < Layers[^1].Size; i++)
            errors[i] = targets[i] - Layers[^1].Neurons[i];

        for (var k = Layers.Length - 2; k >= 0; k--)
        {
            var layer = Layers[k];
            var l1 = Layers[k + 1];

            var errorsNext = new double[layer.Size];
            var gradients = new double[l1.Size];

            for (var i = 0; i < l1.Size; i++)
            {
                gradients[i] = errors[i] * _derivative(Layers[k + 1].Neurons[i]);
                gradients[i] *= _learningRate;
            }

            var deltas = new double[l1.Size, layer.Size];

            for (var i = 0; i < l1.Size; i++)
            for (var j = 0; j < layer.Size; j++)
                deltas[i, j] = gradients[i] * layer.Neurons[j];

            for (var i = 0; i < layer.Size; i++)
            {
                errorsNext[i] = 0;
                for (var j = 0; j < l1.Size; j++)
                    errorsNext[i] += layer.Weights[i, j] * errors[j];
            }

            errors = new double[layer.Size];
            Array.Copy(errorsNext, 0, errors, 0, layer.Size);

            var weightsNew = new double[layer.Weights.GetLength(0), layer.Weights.GetLength(1)];

            for (var i = 0; i < l1.Size; i++)
            for (var j = 0; j < layer.Size; j++)
                weightsNew[j, i] = layer.Weights[j, i] + deltas[i, j];

            layer.Weights = weightsNew;

            for (var i = 0; i < l1.Size; i++)
                l1.Biases[i] += gradients[i];
        }
    }
}