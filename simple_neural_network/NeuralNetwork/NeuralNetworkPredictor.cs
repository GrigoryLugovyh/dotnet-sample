﻿using System.Drawing;
using simple_neural_network.DataPreparer;

namespace simple_neural_network.NeuralNetwork;

public static class NeuralNetworkPredictor
{
    private static SimpleNeuralNetwork _neuralNetwork;

    public static void Init(string modelFile)
    {
        var nn = NeuralNetworkFactory.Create();
        NeuralNetworkStore.Restore(nn, modelFile);

        _neuralNetwork = nn;
    }

    public static string Predict(string imageFile)
    {
        return string.Join("", Preparer.PrepareFile(imageFile).Select(PredictDigit));
    }

    private static int PredictDigit(Bitmap bitmap)
    {
        var inputs = new double[2500];
        for (var x = 0; x < 25; x++)
        for (var y = 0; y < 50; y++)
            inputs[x + y * 50] = (bitmap.GetPixel(x, y).ToArgb() & 0xff) / 255.0;

        var output = _neuralNetwork.Predict(inputs).ToList();

        var predictedDigit = output.IndexOf(output.Max());

        return predictedDigit;
    }
}