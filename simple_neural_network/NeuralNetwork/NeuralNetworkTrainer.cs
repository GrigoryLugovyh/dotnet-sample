﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using simple_neural_network.Utils;

namespace simple_neural_network.NeuralNetwork;

[SuppressMessage("Interoperability", "CA1416:Проверка совместимости платформы")]
public static class NeuralNetworkTrainer
{
    public static SimpleNeuralNetwork Train(SimpleNeuralNetwork neuralNetwork)
    {
        var samples = GetTrainSamples().ToArray(); // 25 x 50

        var inputs = new double[samples.Length, 2500];

        for (var i = 0; i < samples.Length; i++)
        for (var x = 0; x < 25; x++)
        for (var y = 0; y < 50; y++)
            inputs[i, x + y * 50] = (samples[i].image.GetPixel(x, y).ToArgb() & 0xff) / 255.0;

        const int iterations = 2000;

        for (var i = 1; i < iterations; i++)
        {
            var right = 0;
            double errorSum = 0;

            const int batchSize = 100;

            for (var j = 0; j < batchSize; j++)
            {
                var imgIndex = (int) (Random.Shared.NextDouble() * samples.Length);
                var targets = new double[10];
                var digit = samples[imgIndex].digit;

                targets[digit] = 1;

                var outputs = neuralNetwork.Predict(ArrayUtils.GetRow(inputs, imgIndex));

                var maxDigit = 0;
                double maxDigitWeight = -1;

                for (var k = 0; k < 10; k++)
                    if (outputs[k] > maxDigitWeight)
                    {
                        maxDigitWeight = outputs[k];
                        maxDigit = k;
                    }

                if (digit == maxDigit)
                    right++;

                for (var k = 0; k < 10; k++)
                    errorSum += (targets[k] - outputs[k]) * (targets[k] - outputs[k]);

                neuralNetwork.Correct(targets);
            }

            Console.WriteLine("iteration: " + i + " / correct: " + right + " / error: " + errorSum);
        }

        return neuralNetwork;
    }

    private static IEnumerable<(Bitmap image, int digit)> GetTrainSamples()
    {
        foreach (var dir in Directory.GetDirectories("C:\\temp\\class_etalon"))
        {
            var digit = int.Parse(new DirectoryInfo(dir).Name);
            foreach (var file in Directory.GetFiles(dir))
                yield return (BitmapUtils.LoadBitmap(file), digit);
        }
    }
}