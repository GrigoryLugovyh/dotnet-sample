﻿namespace simple_neural_network.NeuralNetwork;

public static class NeuralNetworkFactory
{
    public static SimpleNeuralNetwork Create()
    {
        Func<double, double> sigmoid = x => 1 / (1 + Math.Exp(-x));
        Func<double, double> dsigmoid = y => y * (1 - y);

        return new SimpleNeuralNetwork(0.001, sigmoid, dsigmoid, 2500, 512, 128, 32, 10);
    }
}