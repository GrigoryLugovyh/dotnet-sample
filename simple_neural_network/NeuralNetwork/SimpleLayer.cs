﻿namespace simple_neural_network.NeuralNetwork;

[Serializable]
public class SimpleLayer
{
    public SimpleLayer(int size, int nextSize)
    {
        Size = size;
        Neurons = new double[size];
        Biases = new double[size];
        Weights = new double[size, nextSize];
    }

    public int Size { get; }
    public double[] Biases { get; }
    public double[] Neurons { get; }
    public double[,] Weights { get; set; }
}