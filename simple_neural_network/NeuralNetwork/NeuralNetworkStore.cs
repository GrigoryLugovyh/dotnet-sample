﻿using System.Runtime.Serialization.Formatters.Binary;
using simple_neural_network.Utils;

namespace simple_neural_network.NeuralNetwork;

public static class NeuralNetworkStore
{
    public static void Save(SimpleNeuralNetwork neuralNetwork)
    {
        using var fileStream = new FileStream(
            $"C:\\temp\\saved_models\\{StringUtils.GenerateShortString()}_{DateTime.Now:yyyyMMddhhmmss}",
            FileMode.Create);
        var formatter = new BinaryFormatter();
        formatter.Serialize(fileStream, neuralNetwork.Layers);
    }

    public static void Restore(SimpleNeuralNetwork neuralNetwork, string modelFile)
    {
        using var fileStream = new FileStream(modelFile, FileMode.Open);
        var formatter = new BinaryFormatter();
        var layers = (SimpleLayer[]) formatter.Deserialize(fileStream);
        neuralNetwork.Layers = layers;
    }
}