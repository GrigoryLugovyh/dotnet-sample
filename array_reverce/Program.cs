﻿using System;
using System.Linq;

namespace array_reverce
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = Enumerable.Range(1, 1000).Select(i => i).ToArray();

            var m = a.Length / 2;

            for (var i = 0; i < a.Length; i++)
            {
                if(i >= m) break;

                var j = a.Length - i - 1;

                var f = a[i];
                var l = a[j];

                a[i] = l;
                a[j] = f;
            }

            Console.ReadLine();
        }
    }
}
